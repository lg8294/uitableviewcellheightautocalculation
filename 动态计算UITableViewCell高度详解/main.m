//
//  main.m
//  动态计算UITableViewCell高度详解
//
//  Created by apple on 15/6/16.
//  Copyright (c) 2015年 qisu. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
