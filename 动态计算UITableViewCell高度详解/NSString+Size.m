//
//  NSString+Size.m
//  动态计算UITableViewCell高度详解
//
//  Created by apple on 15/6/17.
//  Copyright (c) 2015年 qisu. All rights reserved.
//

#import "NSString+Size.h"

@implementation NSString (Size)

- (CGSize)calculateSize:(CGSize)size font:(UIFont *)font
{
    CGSize expectedLabelSize = CGSizeZero;
    
    if ([[[UIDevice currentDevice] systemVersion] floatValue] >= 7) {
        
        NSMutableParagraphStyle * paragraphStype = [[NSMutableParagraphStyle alloc] init];
        paragraphStype.lineBreakMode = NSLineBreakByWordWrapping;
        NSDictionary * attributes = @{NSFontAttributeName:font, NSParagraphStyleAttributeName:[paragraphStype copy]};
        
        expectedLabelSize = [self boundingRectWithSize:size options:NSStringDrawingUsesLineFragmentOrigin | NSStringDrawingUsesFontLeading attributes:attributes context:nil].size;
    }
    else {
        expectedLabelSize = [self sizeWithFont:font constrainedToSize:size lineBreakMode:NSLineBreakByWordWrapping];
    }
    
    //ceil(value) 返回不小于 value 的下一个整数，value 如果有小数部分则进一位。
    return CGSizeMake(ceil(expectedLabelSize.width), ceil(expectedLabelSize.height));
}
@end
