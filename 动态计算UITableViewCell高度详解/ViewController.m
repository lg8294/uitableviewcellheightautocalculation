//
//  ViewController.m
//  动态计算UITableViewCell高度详解
//
//  Created by apple on 15/6/16.
//  Copyright (c) 2015年 qisu. All rights reserved.
//

#import "ViewController.h"

#import "C1ViewController.h"
#import "C2TableViewController.h"
#import "C3TableViewController.h"
#import "C4TableViewController.h"
#import "C5TableViewController.h"

@interface ViewController ()

@property (strong, nonatomic) IBOutlet UIButton *c1Btn;
@property (strong, nonatomic) IBOutlet UIButton *c2Btn;
@property (strong, nonatomic) IBOutlet UIButton *c3Btn;
@property (strong, nonatomic) IBOutlet UIButton *c4Btn;

//@property (strong, nonatomic) UITableViewCell * prototypeCell;

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
    
    _c2Btn.titleLabel.numberOfLines = 0;
    
    [self.view needsUpdateConstraints];
}

- (IBAction)processWithC1Btn:(id)sender {
    C1ViewController * c1VC = [[C1ViewController alloc] initWithNibName:@"C1ViewController" bundle:nil];
    
    [self.navigationController pushViewController:c1VC animated:YES];
}
- (IBAction)processWithC2Btn:(id)sender {
    C2TableViewController * c2VC  = [[C2TableViewController alloc] initWithNibName:@"C2TableViewController" bundle:nil];
    [self.navigationController pushViewController:c2VC animated:YES];
}
- (IBAction)processWithC3Btn:(id)sender {
    C3TableViewController * c3VC = [[C3TableViewController alloc] init];
    [self.navigationController pushViewController:c3VC animated:YES];
}
- (IBAction)processWithC4Btn:(id)sender {
    C4TableViewController * c4VC = [[C4TableViewController alloc] init];
    [self.navigationController pushViewController:c4VC animated:YES];
}

- (IBAction)processWithC5Btn:(id)sender {
    C5TableViewController * c5VC = [[C5TableViewController alloc] init];
    [self.navigationController pushViewController:c5VC animated:YES];
}

@end
