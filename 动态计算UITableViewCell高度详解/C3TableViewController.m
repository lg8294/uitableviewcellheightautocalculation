//
//  C3TableViewController.m
//  动态计算UITableViewCell高度详解
//
//  Created by apple on 15/6/17.
//  Copyright (c) 2015年 qisu. All rights reserved.
//

#import "C3TableViewController.h"

#import "C3.h"
#import "NSString+Size.h"

@interface C3TableViewController ()
{
    NSArray * _tableData;
}

@property (strong, nonatomic) UITableViewCell * prototypeCell;

@end

@implementation C3TableViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self initData];
    [self initUserInterface];
}

- (void)initData
{
    _tableData = @[@"1\n2\n3\n4\n5\n6", @"123456789012345678901234567890", @"1\n2", @"1\n2\n3", @"1"];
}
- (void)initUserInterface
{
    [self registerCellWithCellName:@"C3"];
    _prototypeCell = [self.tableView dequeueReusableCellWithIdentifier:@"C3"];
}

- (void)registerCellWithCellName:(NSString *)cellName
{
    [self.tableView registerNib:[UINib nibWithNibName:cellName bundle:nil] forCellReuseIdentifier:cellName];
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {

    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {

    return _tableData.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    C3 *cell = [tableView dequeueReusableCellWithIdentifier:@"C3"];
    cell.t.text = _tableData[indexPath.row];
    
    //使label显示最适应的尺寸
    [cell.t sizeToFit];
    
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView estimatedHeightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 44;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    C3 * cell = (C3 *)_prototypeCell;
    
    NSString * str = _tableData[indexPath.row];
    cell.t.text = str;
    
    CGSize s = [str calculateSize:CGSizeMake(cell.t.frame.size.width, FLT_MAX) font:cell.t.font];
    CGFloat defaultHeight = cell.contentView.frame.size.height;
    
    CGFloat height = s.height > defaultHeight ? s.height : defaultHeight;
    NSLog(@"h = %f, s = %@, deaultHeight = %f", height, NSStringFromCGSize(s), defaultHeight);
    
    return 1 + height;
}

@end
