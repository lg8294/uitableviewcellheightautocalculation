//
//  C2TableViewController.m
//  动态计算UITableViewCell高度详解
//
//  Created by apple on 15/6/16.
//  Copyright (c) 2015年 qisu. All rights reserved.
//

#import "C2TableViewController.h"

#import "C2.h"

@interface C2TableViewController ()
{
    NSArray * _tableData;
}

@property (strong, nonatomic) UITableViewCell * prototypeCell;

@end

@implementation C2TableViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self initData];
    [self initUserInterface];
}

- (void)initData
{
    _tableData = @[@"1\n2\n3\n4\n5\n6", @"123456789012345678901234567890", @"1\n2", @"1\n2\n3", @"1"];
}
- (void)initUserInterface
{
    [self registerCellWithCellName:@"C2"];
    _prototypeCell = [self.tableView dequeueReusableCellWithIdentifier:@"C2"];
}

- (void)registerCellWithCellName:(NSString *)cellName
{
    [self.tableView registerNib:[UINib nibWithNibName:cellName bundle:nil] forCellReuseIdentifier:cellName];
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {

    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {

    return _tableData.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    C2 *cell = [tableView dequeueReusableCellWithIdentifier:@"C2"];
    cell.t.text = _tableData[indexPath.row];
    
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView estimatedHeightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 44;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    C2 *cell = (C2 *)_prototypeCell;
    cell.t.text = _tableData[indexPath.row];
    
    CGSize size = [cell.contentView systemLayoutSizeFittingSize:UILayoutFittingCompressedSize];
    
    CGSize textViewSize = [cell.t sizeThatFits:CGSizeMake(cell.t.frame.size.width, FLT_MAX)];
    

    CGFloat hh = textViewSize.height - 30;
    CGFloat h = size.height + (hh > 0 ? hh : 0);
    
//    h = h > 89 ? h : 89;  //89是图片显示的最低高度， 见xib
    NSLog(@"h=%f, size = %@, textViewSize = %@", h, NSStringFromCGSize(size), NSStringFromCGSize(textViewSize));
    return 1 + h;
}

@end
