//
//  C5TableViewController.m
//  动态计算UITableViewCell高度详解
//
//  Created by apple on 15/6/17.
//  Copyright (c) 2015年 qisu. All rights reserved.
//

#import "C5TableViewController.h"

#import "C5.h"

@interface C5TableViewController ()<UITextViewDelegate>
{
    NSMutableArray * _tableData;
}

@property (strong, nonatomic) UITableViewCell * prototypeCell;

@end

@implementation C5TableViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self initData];
    [self initUserInterface];
}

- (void)initData
{
    _tableData = [NSMutableArray arrayWithArray:@[@"1\n2\n3\n4\n5\n6", @"123456789012345678901234567890", @"1\n2", @"1\n2\n3", @"1"]];
}
- (void)initUserInterface
{
    [self registerCellWithCellName:@"C5"];
    _prototypeCell = [self.tableView dequeueReusableCellWithIdentifier:@"C5"];
}

- (void)registerCellWithCellName:(NSString *)cellName
{
    [self.tableView registerNib:[UINib nibWithNibName:cellName bundle:nil] forCellReuseIdentifier:cellName];
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {

    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {

    return _tableData.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    C5 *cell = [tableView dequeueReusableCellWithIdentifier:@"C5"];
    cell.t.text = _tableData[indexPath.row];
    cell.t.tag = indexPath.row;
    cell.t.delegate = self;
    
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView estimatedHeightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 44;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    C5 * cell = (C5 *)_prototypeCell;
    cell.t.text = _tableData[indexPath.row];
    
    CGSize s = [cell.t sizeThatFits:CGSizeMake(cell.t.frame.size.width, FLT_MAX)];
    CGFloat defaultHeight = cell.contentView.frame.size.height;
    CGFloat height = s.height + 16 > defaultHeight ? s.height + 16 : defaultHeight;
    
    NSLog(@"s = %@, defaultHeight = %f, height = %f", NSStringFromCGSize(s), defaultHeight, height);
    
    return 1 + height;
}

#pragma mark -- UITextViewDelegate
- (BOOL)textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text
{
    if ([text isEqualToString:@"\n"]) {
        NSLog(@"h = %f", textView.contentSize.height);
    }
    return YES;
}
- (void)textViewDidChangeSelection:(UITextView *)textView
{
    
}
- (void)textViewDidChange:(UITextView *)textView
{
    [_tableData replaceObjectAtIndex:textView.tag withObject:textView.text];
    [self.tableView beginUpdates];
    [self.tableView endUpdates];
}
@end
