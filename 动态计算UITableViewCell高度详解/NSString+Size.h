//
//  NSString+Size.h
//  动态计算UITableViewCell高度详解
//
//  Created by apple on 15/6/17.
//  Copyright (c) 2015年 qisu. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@interface NSString (Size)

- (CGSize)calculateSize:(CGSize)size font:(UIFont *)font;

@end
