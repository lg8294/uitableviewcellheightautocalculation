//
//  C1ViewController.m
//  动态计算UITableViewCell高度详解
//
//  Created by apple on 15/6/16.
//  Copyright (c) 2015年 qisu. All rights reserved.
//

#import "C1ViewController.h"
#import "C1.h"

@interface C1ViewController ()<UITableViewDataSource,UITableViewDelegate>
{
    NSArray * _tableData;
}

@property (strong, nonatomic) IBOutlet UITableView *tableView;

@property (strong, nonatomic) UITableViewCell * prototypeCell;

@end

@implementation C1ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    [self initData];
    [self initUserInterface];
}

- (void)initData
{
    _tableData = @[@"1\n2\n3\n4\n5\n6", @"123456789012345678901234567890", @"1\n2", @"1\n2\n3", @"1"];
}
- (void)initUserInterface
{
    
    //    [NSBundle mainBundle] loadNibNamed:<#(NSString *)#> owner:<#(id)#> options:<#(NSDictionary *)#>
    NSArray * cellNames = @[@"C1",@"C2",@"C3",@"C4"];
    
    [self registerCellWithCellName:cellNames[0]];
    _prototypeCell = [_tableView dequeueReusableCellWithIdentifier:cellNames[0]];
}

- (void)registerCellWithCellName:(NSString *)cellName
{
    [_tableView registerNib:[UINib nibWithNibName:cellName bundle:nil] forCellReuseIdentifier:cellName];
}

#pragma mark -- UITableViewDelegate

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return _tableData.count;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSArray * cellNames = @[@"C1",@"C2",@"C3",@"C4"];
    
    //    Class cellClass = NSClassFromString(cellNames[0]);
    
    C1 * cell = [tableView dequeueReusableCellWithIdentifier:cellNames[0]];
    cell.t.text = _tableData[indexPath.row];
    
    return cell;
    //    return nil;
}


- (CGFloat)tableView:(UITableView *)tableView estimatedHeightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 44;
}

#pragma mark -- C1
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    C1 *cell = (C1 *)_prototypeCell;
    
    cell.t.text = [_tableData objectAtIndex:indexPath.row];
    CGSize size = [cell.contentView systemLayoutSizeFittingSize:UILayoutFittingCompressedSize];
    NSLog(@"h=%f", size.height + 1);
    
    return 1  + size.height;
}

@end
